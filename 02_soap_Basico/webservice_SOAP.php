<?php
	require_once "lib/nusoap.php";

	function muestraPlanetas (){
		$planetas =" Según la definición, el sistema solar consta de ocho planetas: Mercurio, Venus, Tierra, Marte, Júpiter, Saturno, Urano y Neptuno. En cambio Plutón, que hasta 2006 se consideraba un planeta, ha pasado a clasificarse como planeta enano, junto a Ceres, también considerado planeta durante algún tiempo, ya que era un referente en la ley de Titius-Bode, y más recientemente considerado como asteroide y Eris, un objeto transneptuniano similar a Plutón";

		return $planetas;
	}
	function muestraImagen ($categoria){
		if ($categoria == 'espacio'){
			$imagen = 'https://i.pinimg.com/originals/7e/64/d4/7e64d4da8503e952c9575c42b0ca37fa.png';
		}else {
			$imagen = "http://quimpi.com/data/gallery/starlight/img/img-20170501-173107.jpg";
		}
		$resultado = '<img src='. $imagen. ' width="300px" heigth="300px">';

		return $resultado;
	}

	if( !isset($HTTP_RAW_POST_DATA) ) {
		$HTTP_RAW_POST_DATA = file_get_contents('php://input');
	}

	$server = new soap_server();
	$server->register("muestraPlanetas");
	$server->register("muestraImagen");

	$server->service($HTTP_RAW_POST_DATA);

?>