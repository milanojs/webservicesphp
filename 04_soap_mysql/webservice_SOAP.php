<?php
//requiere global database;
require_once "dbconn.php";

// Pull in the NuSOAP
require_once "lib/nusoap.php";
/*
function ingresarLibros($Autor, $titulo) {
    $connect = new mysqli("localhost","root","123456789","phpws");
    if ($connect->connect_errno) {
        echo "Failed to connect to MySQL: (" . $connect->connect_errno . ") " . $connect->connect_error;
    }
    if (!$connect->query("INSERT INTO libros SET Autor='$Autor', titulo='$titulo'") ) {
        echo "Table creation failed: (" . $mysqli->errno . ") " . $mysqli->error;
    }

}

function muestraLibros() {
    $connect = new mysqli("localhost","root","123456789","phpws");
    if ($connect->connect_errno) {
        echo "Failed to connect to MySQL: (" . $connect->connect_errno . ") " . $connect->connect_error;
    }

    $res = $connect->query("SELECT * FROM libros ORDER BY id ASC");
    while ($row = mysqli_fetch_array($res,MYSQLI_ASSOC)){
            $todas[] = $row;
    }
    return $todas;
}

// Create the server instance
$server = new soap_server();
// Initialize WSDL support
$server->configureWSDL("Informacion de biblioteca", "urn:infoLibros");
        // Character encoding
        $server->soap_defencoding = 'utf-8';
        //-------------------------------------------------
        //Registrations of our functions
        //-------------------------------------------------
        //Our web service functions will be here.
        //-------------------------------------------------
        $HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';

$server->register(
    'ingresarLibros',   //Name of function
    array('Autor' => 'xsd:string', 'titulo' => 'xsd:string'), //Input Values
    array('return' =>'xsd:boolean'), //Output Values
      'urn:infoLibros',  //Namespace
    'urn:infoLibros#ingresarLibros',  //SoapAction
    'rpc',       //style
    'literal',   //can be encoded but it doesn't work with silverlight
    'Funcion para ingresar libros en la base de datos'
);
$server->register(
    'muestraLibros',
    array(),//parametro
    array('return' =>'xsd:string'),
    'urn:infoLibros',
    'urn:infoLibros#muestraLibros',
    'rpc',
    'literal',
    'Some comments about function 2'
);

*/

function muestraLibros()
{
    global $connect;
    $query = "SELECT * FROM libro ORDER BY id ASC" ;
    $res = $connect-> query($query);
    if (!$res) {
        $error = "Error description: " . mysqli_error($connect);
        return $error;
    } else {
        while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
            $todas[] = $row;
        }
        global $todas;
        $libros = json_encode($todas);
        return $libros;
    }
    mysqli_close($connect);
}
muestraLibros();
function muestraImagen($categoria)
{
    if ($categoria == 'espacio') {
        $imagen = "img/201x200.png";
    } else {
        $imagen = "img/200x201.png";
    }
    $resultado = '<img src='. $imagen. ' width="300px" heigth="300px">';

    return $resultado;
}

if (!isset($HTTP_RAW_POST_DATA)) {
    $HTTP_RAW_POST_DATA = file_get_contents('php://input');
}

$server = new soap_server();
$server->configureWSDL("Info Blog", "urn:infoBlog");

$server->register(
    "muestraLibros",
    array(),//parametro
    array('return' => 'xsd:string'), //respuesta
    'urn:infoBlog', //namespace
    'urn:infoBlog#muestraLibros', //accion
    'rpc', //estilo
    'encoded', //uso
    'muestra el contenido del blog'
); //descripcion

$server->register(
    "muestraImagen",
    array('categoria' => 'xsd:string' ),
    array('return' => 'xsd:string'), //respuesta
    'urn:infoBlog', //namespace
    'urn:infoBlog#muestraImagen', //accion
    'rpc', //estilo
    'encoded', //uso
    'muestra imagen solicitada por la categoria'
);

$server->service($HTTP_RAW_POST_DATA);
