<?php

require_once "lib/nusoap.php";

    // 	// set the URL or path to the WSDL document
    $wsdl = "http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL";

     // instantiate the SOAP client object
    $cliente = new soapclient($wsdl, "wsdl");

     // get the SOAP proxy object, which allows you to call the methods directly
    $proxy = $cliente->getProxy();
    // set parameter parameters (CurrencyName^)
    $parameters = array('sCountryISOCode'=>"VEB");

    // get the result, a native PHP type, such as an array or string
    $result = $proxy->FullCountryInfo($parameters);

    echo "<h3>La informacion del pais solicitado</h3>";
    echo "Nombre del pais: <strong>" . $result['FullCountryInfoResult']['sName']."</strong>";
    echo "</br>";
    echo "Capital: <strong>" . $result['FullCountryInfoResult']['sCapitalCity'] ."<strong>";



 unset($cliente);
