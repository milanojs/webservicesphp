<?php
    header('Content-Type: application/json');


    function mostar_alumnos()
    {
        $alumno = array(
            'nombre' => 'Juan',
            'apellido' => 'Milano',
            'pais' => 'Venezuela',
            'cursos' => '5',
            'usuario' => 'juanito',
        );
        return json_encode($alumno);
    }
