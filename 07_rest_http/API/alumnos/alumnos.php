<?php
// header('Content-Type: application/json');
function mostar_cursos()
{
    $cursos = array(
            'AngularJS','MongoDB','PHP','Rails','UX'
        );
    return $cursos;
}


function mostar_alumnos()
{
    $alumno = array( 'Juan','Antonio','Pedro','Raul','Maria'
    );
    return $alumno;
}
if ($_GET['solicitud'] == 'cursos') {
    $resultados = mostar_cursos();
} elseif ($_GET['solicitud'] == 'lista') {
    $resultados = mostar_alumnos();
} else {
    header('HTTP/1.1 405 Method Not Allowed');
    exit;
}

echo json_encode($resultados);
