<?php

//requiere global database;
require_once "dbconn.php";

// header('Content-Type: application/json');
function mostar_titulos($detalle)
{
    if ($detalle =="lista") {
        $query = "SELECT titulo FROM libro ORDER BY id ASC" ;
        global $connect;
        $res = $connect-> query($query);
        if (!$res) {
            $error = "Error description: " . mysqli_error($connect);
            return $error;
        } else {
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $todas[] = $row;
            }
            $resultados = json_encode($todas);
            return $resultados;
        }
    } else {
        $query = "SELECT titulo FROM libro where id=". $detalle ;

        global $connect;
        $res = $connect-> query($query);
        if (!$res) {
            $error = "Error description: " . mysqli_error($connect);
            return $error;
        } else {
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $todas[] = $row;
            }
            $resultados = json_encode($todas);
            return $resultados;
        }
    }
}
function mostar_autor($detalle)
{
    if ($detalle =="lista") {
        $query = "SELECT autor FROM libro ORDER BY id ASC" ;
        global $connect;
        $res = $connect-> query($query);
        if (!$res) {
            $error = "Error description: " . mysqli_error($connect);
            return $error;
        } else {
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $todas[] = $row;
            }
            $resultados = json_encode($todas);
            return $resultados;
        }
    } else {
        $query = "SELECT autor FROM libro where id=". $detalle ;

        global $connect;
        $res = $connect-> query($query);
        if (!$res) {
            $error = "Error description: " . mysqli_error($connect);
            return $error;
        } else {
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $todas[] = $row;
            }
            $resultados = json_encode($todas);
            return $resultados;
        }
    }
}

if ($_GET['peticion'] == 'titulo') {
    echo mostar_titulos($_GET['detalle']);
    $resultados = mostar_titulos($_GET['detalle']);
} elseif ($_GET['peticion'] == 'autor') {
    echo $resultados = mostar_autor($_GET['detalle']);
} else {
    header('HTTP/1.1 405 Method Not Allowed');
    exit;
}
