<?php

//requiere global database;
require_once "dbconn.php";

header('Content-Type: application/json');
header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods:POST, GET, OPTIONS');



function mostar_titulos($detalle)
{
    if ($detalle =="lista") {
        $query = "SELECT titulo FROM libro ORDER BY id ASC" ;
        global $connect;
        $res = $connect-> query($query);
        if (!$res) {
            $error = "Error description: " . mysqli_error($connect);
            return $error;
        } else {
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $todas[] = $row;
            }
            $resultados = json_encode($todas);
            return $resultados;
        }
    } else {
        $query = "SELECT titulo FROM libro where id=". $detalle ;

        global $connect;
        $res = $connect-> query($query);
        if (!$res) {
            $error = "Error description: " . mysqli_error($connect);
            return $error;
        } else {
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $todas[] = $row;
            }
            $resultados = json_encode($todas);
            return $resultados;
        }
    }
}
function mostar_autor($detalle)
{
    if ($detalle =="lista") {
        $query = "SELECT autor FROM libro ORDER BY id ASC" ;
        global $connect;
        $res = $connect-> query($query);
        if (!$res) {
            $error = "Error description: " . mysqli_error($connect);
            return $error;
        } else {
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $todas[] = $row;
            }
            $resultados = json_encode($todas);
            return $resultados;
        }
    } else {
        $query = "SELECT autor FROM libro where id=". $detalle ;

        global $connect;
        $res = $connect-> query($query);
        if (!$res) {
            $error = "Error description: " . mysqli_error($connect);
            return $error;
        } else {
            while ($row = mysqli_fetch_array($res, MYSQLI_ASSOC)) {
                $todas[] = $row;
            }
            $resultados = json_encode($todas);
            return $resultados;
        }
    }
}
function guardar_nuevo_libro()
{
    global $connect;

    $autor=$_POST['autor'];
    $titulo=$_POST['titulo'];

    $query = "INSERT INTO libro ( autor,titulo ) VALUES ('".$autor."','". $titulo."')";
    $res = $connect-> query($query);

    if (!$res) {
        $error = "Error description: " . mysqli_error($connect);
        return $error;
    } else {
        echo "se inserto Correctamente";
        header('location:../../../');
        exit;
    }
}

if ($_GET['peticion'] == 'titulo') {
    echo mostar_titulos($_GET['detalle']);
    $resultados = mostar_titulos($_GET['detalle']);
} elseif ($_GET['peticion'] == 'autor') {
    if ($_GET['detalle']== 'nuevo') {
        guardar_nuevo_libro();
    } else {
        echo $resultados = mostar_autor($_GET['detalle']);
    }
} else {
    header('HTTP/1.1 405 Method Not Allowed');
    exit;
}
