<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sistemas de bibliotecas</title>
</head>
<body>
    <h1>Sistema Local de bibliotecas</h1>

    <div id="resultadosLibro">
        <ul></ul>
    </div>
    <button id="libros">Libros Disponibles</button>
    <div id="resultadosAutor">
        <ul></ul>
    </div>
    <button id="autor">Autores</button>
    <script type="text/javascript" src="js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="js/app.js"></script>
</body>
</html>
